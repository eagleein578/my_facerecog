This is a set of python scripts to do face extraction, tagging and search.
The project is a fork of https://github.com/wsthub/python_facial_recognition/ .

4 files are used for
1. extract_unknown_face.py: extract unknown face from images in img/unhandled and put into img/unknown_faces
2. tag_face.py: a streamlit web server to tag faces in img/unknown_faces
3. list_face.py: list known face in a picture
4. camera_face_recognize.py: detect known faces from webcam realtime image


A U.S ex-president Trump photo is in img/unhandled as a demo example. 
A typical trial process is show below:


```
conda create -n opencv python=3.8
conda activate opencv
git clone https://gitlab.com/eagleein578/my_facerecog.git
cd my_facerecog
pip install -r requirements.txt
python extract_unknown_face.py
streamlit run tag_face.py --server.headless true # any browse http://127.0.0.1:8501 for tagging
python list_face.py img\handled\71efab46a60de0859084b4b083c989a9.jpg # search known faces
python camera_face_recognize.py # for webcam face detection
```
