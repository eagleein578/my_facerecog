# coding=UTF-8
import os
os.add_dll_directory(os.path.join(os.environ['CUDA_PATH'], 'bin'))
import face_recognition
import dlib
import cv2
import sys
import random
import shutil
import math

KNOWN_FACES_DIR =  "./img/known_faces" # training data
UNKNOWN_FACES_DIR = "./img/unknown_faces"
UNHANDLED_PIC_DIR = "./img/unhandled"
HANDLED_PIC_DIR = "./img/handled"

TOLERANCE = 0.5
MODEL = "hog" # convolutional
known_faces = [] # store known faces here
known_names = [] # store name of them here

print(dlib.DLIB_USE_CUDA) # ture, if cuda is enabled.

### TRAIN THE CNN MODEL ON KNOWN FACES #############################
for name in os.listdir(KNOWN_FACES_DIR):
    if len(name) > 0 and name[0] == ".":
        continue
    if name == "junk":
        continue
    for filename in os.listdir(f"{KNOWN_FACES_DIR}/{name}"):

        image = face_recognition.load_image_file(f"{KNOWN_FACES_DIR}/{name}/{filename}")
        print("Known face: " +  f"{KNOWN_FACES_DIR}/{name}/{filename}")
        encoding = face_recognition.face_encodings(image, model='large')
        if len(encoding) < 1:
            continue
        else:
            encoding=encoding[0]
        known_faces.append(encoding)
        known_names.append(name)
####################################################################

faces=0

for filename in os.listdir(f"{UNHANDLED_PIC_DIR}/"):
    if len(filename) > 0 and filename[0] == ".":
        continue
    image = cv2.imread(f"{UNHANDLED_PIC_DIR}/{filename}")
    locations = face_recognition.face_locations(image, model=MODEL)
    encodings = face_recognition.face_encodings(image, locations, model='large')
    print(str(len(locations)) + " Faces detected in " + f"{UNHANDLED_PIC_DIR}/{filename}")
    known=0
    unknown=0
    for face_encoding, face_location in zip(encodings, locations):
        results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE)
        match = None
        if True in results: # if there is a known face
            # match = known_names[results.index(True)] # who that person is
            #print(f"Match found: {match}")
            known = known+1
        else:
            lx = face_location[3];
            ly =face_location[0];
            rx = face_location[1]
            ry = face_location[2]
            roi = image[ly:ry, lx:rx]
            newname = str(abs(hash(tuple(face_encoding))))+".jpg"
            cv2.imwrite(f"{UNKNOWN_FACES_DIR}/{newname}", roi)
            unknown = unknown+1
    print(f"Known={known} Unknown={unknown}")


    shutil.move(f"{UNHANDLED_PIC_DIR}/{filename}", f"{HANDLED_PIC_DIR}/{filename}")


