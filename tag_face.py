# -*- coding: utf-8 -*-
import streamlit as st
from PIL import Image
import os
import shutil

def movefile(src, dstfolder, dstfn):
    try:
        if len(dstfolder) > 0:
            os.mkdir(dstfolder)
    except:
        pass
    try:
        shutil.move(src, dstfolder+"/"+dstfn)
    except:
        pass

    try:
        os.remove(src)
    except:
        pass

vars={}
COLS=6
for i in range(COLS):
    vars[f"FaceLabel{i}"]=""
    vars[f"FaceSelect{i}"]="None"
    if f"FaceSelect{i}" in st.session_state:
        vars[f"FaceSelect{i}"] = st.session_state[f"FaceSelect{i}"]
    st.session_state[f"FaceSelect{i}"]="None"

    if f"FaceLabel{i}" in st.session_state:
        vars[f"FaceLabel{i}"] = st.session_state[f"FaceLabel{i}"]
    st.session_state[f"FaceLabel{i}"]=""

UNKNOWN_FACES_DIR = "./img/unknown_faces"
KNOWN_FACES_DIR = "./img/known_faces"



st.title('Face Tagging')
image_set=[]
faces=[]
i=0
faces.append("None")
cols = st.columns(COLS)
# 認識的臉
for dirname in os.listdir(f"{KNOWN_FACES_DIR}/"):
    if dirname[0] == ".":
        continue
    faces.append(dirname)

# 待辨識的臉
for filename in os.listdir(f"{UNKNOWN_FACES_DIR}/"):
    if filename[0] == ".":
        continue
    image_set.append(f"{UNKNOWN_FACES_DIR}/{filename}")

if len(image_set) == 0:
    st.write("No unhandled photo")

for i in range(len(image_set)):
    if i>= (COLS-1):
        break
    with cols[i]:
        img = st.empty()
        optionsEmpty = st.empty()
        otherOptionsEmpty = st.empty()

        fn = image_set[i]
        image = Image.open(image_set[i])
        img.image(image, width=100)


        optionsEmpty.selectbox('人物', faces, key=f"FaceSelect{i}")
        otherOptionsEmpty.text_input("新增" , key=f"FaceLabel{i}")

        if st.button("不認識",key=f"FaceBtn{i}"):
            movefile(fn, f"{KNOWN_FACES_DIR}/junk", "")
            print("move to junk")

        elif len(vars[f'FaceLabel{i}']) > 0:
            movefile(fn, f"{KNOWN_FACES_DIR}/"+vars[f'FaceLabel{i}'], "")
            print("move by label")

        elif vars[f'FaceSelect{i}'] != "None":
            movefile(fn, f"{KNOWN_FACES_DIR}/"+vars[f'FaceSelect{i}'], "")
            print("move by select")
        else: #no activity
            continue
        st.rerun()





