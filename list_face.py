# coding=UTF-8
import os
os.add_dll_directory(os.path.join(os.environ['CUDA_PATH'], 'bin'))
import dlib
import face_recognition
import os
import cv2
import sys
import random
import shutil
import math
import sys

KNOWN_FACES_DIR =  "./img/known_faces" # training data
UNKNOWN_FACES_DIR = "./img/unknown_faces"
UNHANDLED_PIC_DIR = "./img/unhandled"
HANDLED_PIC_DIR = "./img/handled"

TOLERANCE = 0.5
MODEL = "hog" # convolutional
known_faces = [] # store known faces here
known_names = [] # store name of them here

print(dlib.DLIB_USE_CUDA) # ture, if cuda is enabled.

### TRAIN THE CNN MODEL ON KNOWN FACES #############################
for name in os.listdir(KNOWN_FACES_DIR):
    if name[0] == ".":
        continue
    if name == "junk":
        continue
    for filename in os.listdir(f"{KNOWN_FACES_DIR}/{name}"):

        image = face_recognition.load_image_file(f"{KNOWN_FACES_DIR}/{name}/{filename}")
        #print("Known face: " +  f"{KNOWN_FACES_DIR}/{name}/{filename}")
        encoding = face_recognition.face_encodings(image, model='large')
        if len(encoding) < 1:
            continue
        else:
            encoding=encoding[0]
        known_faces.append(encoding)
        known_names.append(name)
####################################################################

faces=0

image = cv2.imread(sys.argv[1])
locations = face_recognition.face_locations(image, model=MODEL)
encodings = face_recognition.face_encodings(image, locations, model='large')
print("共有 " + str(len(locations)) + " 個面孔")
for face_encoding, face_location in zip(encodings, locations):
    results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE)
    match = None
    if True in results: # if there is a known face
        match = known_names[results.index(True)] # who that person is
        faces = faces + 1

        print(f"找到: {match}")

if faces == 0:
    print("沒發現")




